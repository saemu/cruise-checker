package ch.samuelbrand.cruisechecker.service;

import ch.samuelbrand.cruisechecker.model.Cabin;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CabinConsumerServiceImpl implements CabinConsumerService {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final String apiKey;
    private final WebClient webClient;

    public CabinConsumerServiceImpl(WebClient webClient, @Value("${api.key}") String apiKey) {
        this.apiKey = apiKey;
        this.webClient = webClient;
    }

    private Object[] getReponse(String voyage, String category) {
        Mono<Object[]> reponse = webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path(voyage + "/cabins")
                        .queryParam("cabinCategory", category)
                        .build())
                .header("x-api-key", apiKey)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(Object[].class).log();
        return reponse.block();
    }

    @Override
    public List<String> getCategories() {
        return Arrays.asList("VA", "VB", "VC", "VD", "VE", "VF", "VG", "VH", "VP");
    }

    @Override
    public List<Cabin> readCabinsFromArray(String voyage) {
        List<Cabin> cabinList = new ArrayList<>();
        for (String category : getCategories()) {
            cabinList.addAll(readCabinsFromArray(voyage, category));
        }
        return cabinList;
    }

    @Override
    public List<Cabin> readCabinsFromArray(String voyage, String category) {
        Object[] objects = getReponse(voyage, category);
        return Arrays.stream(objects)
                .map(object -> objectMapper.convertValue(object, Cabin.class))
                .toList();
    }
}
