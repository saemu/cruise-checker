package ch.samuelbrand.cruisechecker.service;

import ch.samuelbrand.cruisechecker.model.Cabin;

import java.util.List;

public interface CabinConsumerService {

    List<Cabin> readCabinsFromArray(String voyage);

    List<Cabin> readCabinsFromArray(String voyage, String category);

    List<String> getCategories();
}
