package ch.samuelbrand.cruisechecker;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

@Push
@Theme(value = "cruisechecker", variant = Lumo.DARK)
@PWA(name = "Cruise Checker", shortName = "Cruise Checker", offlineResources = {})
public class AppShell implements AppShellConfigurator {
}
