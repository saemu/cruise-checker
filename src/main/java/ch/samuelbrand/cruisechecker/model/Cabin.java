package ch.samuelbrand.cruisechecker.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "cabinCategory",
        "mainCabinCategory",
        "id",
        "minPerson",
        "maxPerson",
        "area",
        "babyBed",
        "balcony",
        "balconyType",
        "bed",
        "cailingPullman",
        "closeToRestaurant",
        "closeToStairs",
        "closeToWellness",
        "connection",
        "connectionDoor",
        "deck",
        "directionOfTravel",
        "doubleBed",
        "information",
        "isAccessible",
        "layout",
        "location",
        "porthole",
        "position",
        "sepBeds",
        "sepSundeck",
        "sofaBed",
        "twoBathrooms",
        "view",
        "visibility",
        "walkinWardrobe",
        "wallPullman"
})
@Generated("jsonschema2pojo")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Cabin {

    @JsonProperty("cabinCategory")
    @Getter
    private String cabinCategory;

    @JsonProperty("mainCabinCategory")
    @Getter
    private String mainCabinCategory;

    @JsonProperty("id")
    @Getter
    private Integer id;

    @JsonProperty("minPerson")
    @Getter
    private Integer minPerson;

    @JsonProperty("maxPerson")
    @Getter
    private Integer maxPerson;

    @JsonProperty("area")
    @Getter
    private Double area;

    @JsonProperty("babyBed")
    @Getter
    private Boolean babyBed;

    @JsonProperty("balcony")
    @Getter
    private Double balcony;

    @JsonProperty("balconyType")
    @Getter
    private String balconyType;

    @JsonProperty("bed")
    @Getter
    private Integer bed;

    @JsonProperty("cailingPullman")
    @Getter
    private Integer cailingPullman;

    @JsonProperty("closeToRestaurant")
    @Getter
    private Boolean closeToRestaurant;

    @JsonProperty("closeToStairs")
    @Getter
    private Boolean closeToStairs;

    @JsonProperty("closeToWellness")
    @Getter
    private Boolean closeToWellness;

    @JsonProperty("connection")
    @Getter
    private Integer connection;

    @JsonProperty("connectionDoor")
    @Getter
    private Boolean connectionDoor;

    @JsonProperty("deck")
    @Getter
    private Integer deck;

    @JsonProperty("directionOfTravel")
    @Getter
    private Boolean directionOfTravel;

    @JsonProperty("doubleBed")
    @Getter
    private Integer doubleBed;

    @JsonProperty("information")
    @Getter
    private Object information;

    @JsonProperty("isAccessible")
    @Getter
    private Boolean isAccessible;

    @JsonProperty("layout")
    @Getter
    private String layout;

    @JsonProperty("location")
    @Getter
    private String location;

    @JsonProperty("porthole")
    @Getter
    private Integer porthole;

    @JsonProperty("position")
    @Getter
    private String position;

    @JsonProperty("sepBeds")
    @Getter
    private Integer sepBeds;

    @JsonProperty("sepSundeck")
    @Getter
    private Boolean sepSundeck;

    @JsonProperty("sofaBed")
    @Getter
    private Integer sofaBed;

    @JsonProperty("twoBathrooms")
    @Getter
    private Boolean twoBathrooms;

    @JsonProperty("view")
    @Getter
    private String view;

    @JsonProperty("visibility")
    @Getter
    private String visibility;

    @JsonProperty("walkinWardrobe")
    @Getter
    private Boolean walkinWardrobe;

    @JsonProperty("wallPullman")
    @Getter
    private Integer wallPullman;

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Cabin)) {
            return false;
        }
        return id == ((Cabin) obj).id;
    }


    public String getMainCategoryString() {
        Map<String, String> categories = new HashMap<>();
        categories.put("K", "Veranda komfort");
        categories.put("V", "Veranda");
        return categories.get(mainCabinCategory);
    }
    public String getCabinCategoryString() {
        Map<String, String> categories = new HashMap<>();
        categories.put("VA", "bevorzugte Lage mit 2 getrennten Bädern");
        categories.put("VB", "bevorzugte Lage mit begehbarem Kleiderschrank");
        categories.put("VC", "bevorzugte Lage mit extra viel Platz");
        categories.put("VD", "mit 2 getrennten Bädern");
        categories.put("VE", "mit begehbarem Kleiderschrank");
        categories.put("VF", "mit extra viel Platz");
        categories.put("VG", "bevorzugte Lage");
        categories.put("VH", "Standard");
        categories.put("VP", "Panoramakabine mit Zugang zum Patiodeck");
        return categories.get(cabinCategory);
    }
}