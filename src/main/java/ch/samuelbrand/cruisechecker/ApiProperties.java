package ch.samuelbrand.cruisechecker;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="api")
public class ApiProperties {
    @Getter
    @Setter
    private String url;
    @Getter
    @Setter
    private String key;
}
