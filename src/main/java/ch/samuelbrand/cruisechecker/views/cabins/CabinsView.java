package ch.samuelbrand.cruisechecker.views.cabins;

import ch.samuelbrand.cruisechecker.model.Cabin;
import ch.samuelbrand.cruisechecker.service.CabinConsumerService;
import ch.samuelbrand.cruisechecker.views.MainLayout;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@Scope("prototype")
@PageTitle("Cabins")
@Route(value = "cabins", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
@Slf4j
public class CabinsView extends VerticalLayout {
    private static final String DETAILS_VIEW = "details-view";

    private CabinConsumerService service;
    private Grid<Cabin> cabinsGrid = new Grid<>(Cabin.class);
    private CabinView cabinView = new CabinView();
    private ComboBox<String> categories;
    private ComboBox<String> location;
    private ComboBox<String> position;
    private ComboBox<Integer> decks;

    private List<Cabin> cabinList;
    private TextField voyage;

    public CabinsView(@Autowired CabinConsumerService service) {
        this.service = service;
        addClassName("cabins-view");
        setSizeFull();
        configureGrid();

        configureDetailsView();

        FlexLayout layout = new FlexLayout(cabinsGrid, cabinView);
        layout.setFlexGrow(2.0, cabinsGrid);
        layout.setFlexGrow(1.0, cabinView);
        layout.setFlexShrink(0.0, cabinView);
        layout.addClassNames("content", "gap-m");
        layout.setSizeFull();

        add(toolbar(), layout);

        closeDetailsView();

        cabinsGrid.asSingleSelect().addValueChangeListener(event ->
                updateDetailsView(event.getValue()));
    }

    private void filterCabins() {
        List<Cabin> filteredList = cabinList.stream()
                .filter(cabin -> filterCabin(categories.getValue(), cabin.getCabinCategory()))
                .filter(cabin -> filterCabin(location.getValue(), cabin.getLocation()))
                .filter(cabin -> filterCabin(position.getValue(), cabin.getPosition()))
                .filter(cabin -> filterByDeck(decks.getValue(), cabin.getDeck()))
                .toList();
        log.info("showing cabins {}", filteredList.size());
        cabinsGrid.setItems(filteredList);
    }

    private com.vaadin.flow.component.Component toolbar() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.addClassName("toolbar");

        voyage = new TextField("Reise");
        voyage.addClassName("voyage-selection");
        voyage.setPlaceholder("Bitte Reise Nummer eingeben...");
        voyage.addValueChangeListener(e -> readCabins());

        categories = new ComboBox<>("Kategorie");
        categories.addClassName("categorie-filter");
        categories.setItems(service.getCategories());
        categories.addValueChangeListener(e -> filterCabins());

        location = new ComboBox<>("Lage", "backbord", "steuerbord");
        location.addClassName("location-filter");
        location.addValueChangeListener(e -> filterCabins());

        position = new ComboBox<>("Position", "vorne", "mittig", "hinten");
        position.addClassName("position-filter");
        position.addValueChangeListener(e -> filterCabins());

        decks = new ComboBox<>("Deck", 5, 8, 9, 10, 11, 12, 14, 15, 16);
        decks.addClassName("deck-filter");
        decks.addValueChangeListener(e -> filterCabins());

        layout.add(voyage, categories, location, position, decks);
        return layout;
    }

    private void readCabins() {
        this.cabinList = this.service.readCabinsFromArray(voyage.getValue());
        log.info("available cabins {}", cabinList.size());
        filterCabins();
    }

    private boolean filterCabin(String filter, String value) {
        if (null == filter) {
            return true;
        }
        return Objects.equals(filter, value);
    }

    private boolean filterByDeck(Integer filter, Integer deck) {
        if (null == filter) {
            return true;
        }
        return Objects.equals(filter, deck);
    }

    private void configureGrid() {
        cabinsGrid.addClassNames("cabins-grid");
        cabinsGrid.setSizeFull();
        cabinsGrid.removeAllColumns();
        cabinsGrid.addColumn(Cabin::getCabinCategory)
                .setHeader("Kategorie")
                .setSortable(true);
        cabinsGrid.addColumn(Cabin::getId)
                .setHeader("Nummer")
                .setSortable(true);
        cabinsGrid.getColumns().forEach(col -> col.setAutoWidth(true));
    }

    private void configureDetailsView() {
        cabinView.setWidth("66%");
        cabinView.addListener(CabinView.CloseEvent.class, e -> closeDetailsView());
    }

    private void closeDetailsView() {
        cabinView.setCabin(null);
        cabinView.setVisible(false);
        removeClassName(DETAILS_VIEW);
    }
    private void updateDetailsView(Cabin cabin) {
        if (cabin == null) {
            closeDetailsView();
            return;
        }
        cabinView.setCabin(cabin);
        cabinView.setVisible(true);
        addClassName(DETAILS_VIEW);
    }
}
