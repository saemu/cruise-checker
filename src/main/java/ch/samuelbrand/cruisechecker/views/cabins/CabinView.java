package ch.samuelbrand.cruisechecker.views.cabins;

import ch.samuelbrand.cruisechecker.model.Cabin;
import ch.samuelbrand.cruisechecker.views.MainLayout;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToFloatConverter;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.shared.Registration;
import lombok.Getter;

public class CabinView extends FormLayout {

    Cabin cabin;

    Binder<Cabin> binder = new BeanValidationBinder<>(Cabin.class);
    TextField mainCabinCategory = new TextField("Kabinentyp");
    TextField cabinCategory = new TextField("Kabinenkategorie");
    IntegerField id = new IntegerField("Kabinennummer");
    TextField location = new TextField("Lage");
    TextField position = new TextField("Position");
    NumberField area = new NumberField("Grösse");
    NumberField balcony = new NumberField("Balkongrösse");
    TextField balconyType = new TextField("Balkonart");

    Image layout = new Image();

    public CabinView() {
        addClassNames("cabin-view");

        layout.getStyle().set("padding", "10px");

        add(id,
            mainCabinCategory,
            cabinCategory,
            location,
            position,
            area,
            balcony,
            balconyType,
            layout
        );

        setColspan(id, 2);
        setColspan(layout, 2);

        configureAreaField(area);
        configureAreaField(balcony);



        binder.bindInstanceFields(this);
        binder.forField(mainCabinCategory)
                .bind(Cabin::getMainCategoryString, null);
        binder.forField(cabinCategory)
                .bind(Cabin::getCabinCategoryString, null);
    }

    private void configureAreaField(NumberField field) {
        field.setSuffixComponent(createSquareMeters());
        field.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);
    }

    private Component createSquareMeters() {
        Div squareMeters = new Div();
        squareMeters.setText("m²");
        return squareMeters;
    }

    public void setCabin(Cabin cabin) {
        this.cabin = cabin;
        binder.readBean(cabin);
        if (null != cabin) {
            layout.setSrc(cabin.getLayout());
        }
    }

    public static class CloseEvent extends ComponentEvent<CabinView> {
        public CloseEvent(CabinView source) {
            super(source, false);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType, ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
