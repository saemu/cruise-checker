package ch.samuelbrand.cruisechecker;

import com.vaadin.flow.component.dependency.NpmPackage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@NpmPackage(value = "line-awesome", version = "1.3.0")
@Slf4j
@EnableConfigurationProperties(ApiProperties.class)
public class CruiseCheckerApplication {

    private final ApiProperties properties;

    public CruiseCheckerApplication(ApiProperties properties) {
        this.properties = properties;
    }

    public static void main(String[] args) {
        SpringApplication.run(CruiseCheckerApplication.class, args);
    }

    @Bean
    WebClient webClient() {
        return WebClient.create(properties.getUrl());
    }
}
