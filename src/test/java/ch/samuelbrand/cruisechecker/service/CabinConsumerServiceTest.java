package ch.samuelbrand.cruisechecker.service;

import ch.samuelbrand.cruisechecker.model.Cabin;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CabinConsumerServiceTest {

    private static final String CABIN_REPONSE = """
[
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "9204",
    "minPerson": 1,
    "maxPerson": 2,
    "area": 20.5,
    "babyBed": true,
    "balcony": 16,
    "balconyType": "stahl",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 9,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/Stahl/BB-X-2_v001_stahl.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 0,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath%3A..%2Fde_00326%2F%2Cfile%3Aperla_9203%2Clanguage%3Ade",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath%3A..%2Fde_00326%2F%2Cfile%3Aperla_9203%2Clanguage%3Ade",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "12170",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.4,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 12,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "steuerbord",
    "porthole": 0,
    "position": "hinten",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "12215",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.2,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 12,
    "directionOfTravel": true,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "14206",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.2,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 14,
    "directionOfTravel": true,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15226",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.8,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": true,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "mittig",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15122",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 7,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": true,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "steuerbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15222",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 7,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": true,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15204",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 7,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": true,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15105",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.8,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "steuerbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15217",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.8,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": true,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15118",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.8,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "steuerbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  },
  {
    "cabinCategory": "VC",
    "mainCabinCategory": "K",
    "id": "15218",
    "minPerson": 1,
    "maxPerson": 3,
    "area": 20.5,
    "babyBed": true,
    "balcony": 6.8,
    "balconyType": "glas",
    "bed": 0,
    "cailingPullman": 0,
    "closeToRestaurant": false,
    "closeToStairs": false,
    "closeToWellness": false,
    "connection": 0,
    "connectionDoor": false,
    "deck": 15,
    "directionOfTravel": false,
    "doubleBed": 1,
    "information": null,
    "isAccessible": false,
    "layout": "https://media.aida.de/fileadmin/user_upload/dam/cabinfinder/cabins/aida/PE/VF_VC/AB_iW3_v001.jpg",
    "location": "backbord",
    "porthole": 0,
    "position": "vorne",
    "sepBeds": 0,
    "sepSundeck": false,
    "sofaBed": 1,
    "twoBathrooms": false,
    "view": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "visibility": "https://www.aida.de/fileadmin/user_upload_aws/360/_/index.php?options=projectPath:../de_00175/,file:aida_prima_8129,language:de",
    "walkinWardrobe": false,
    "wallPullman": 0
  }
]
""";

    private static final String BASE_URL = "http://localhost:8080/cabins";
    private static final String API_KEY = "SOME_DUMMY_KEY";

    WebClient webClientMock = WebClient.builder()
            .baseUrl(BASE_URL)
            .exchangeFunction(clientRequest -> Mono.just(ClientResponse.create(HttpStatus.OK)
                    .header("content-type", "application/json")
                    .body(CABIN_REPONSE)
                    .build()))
            .build();

    private final CabinConsumerService tested = new CabinConsumerServiceImpl(webClientMock, API_KEY);

    @Test
    void when_readCabinFromArray_then_OK() {
        List<Cabin> cabins = tested.readCabinsFromArray("dummy", "VC");
        assertThat(cabins).isNotEmpty();
    }

}